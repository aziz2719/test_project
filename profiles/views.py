from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from .models import Profile
from .serializers import ProfileSerializer

class ProfileView(ModelViewSet):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    lookup_field = 'pk'