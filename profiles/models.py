from django.db import models

class Profile(models.Model):
    customer = models.ForeignKey('users.Customer', models.SET_NULL, related_name='profile_customer', null=True)
    courier = models.ForeignKey('users.Courier', models.SET_NULL, related_name='profile_courier', null=True)
    date = models.DateField('Дата рождения')#, auto_now_add=True
    balance = models.PositiveSmallIntegerField('Баланс', default=0)