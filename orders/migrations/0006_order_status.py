# Generated by Django 3.1.7 on 2021-03-18 17:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0005_auto_20210318_2031'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='status',
            field=models.CharField(choices=[('W', 'Wait'), ('M', 'Order is made'), ('R', 'Rejected')], default=1, max_length=100),
            preserve_default=False,
        ),
    ]
