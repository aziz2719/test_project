from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from .models import Order, OrderProduct
from .serializers import OrderSerializer, OrderProductSerializer
from rest_framework.permissions import IsAuthenticated


class OrderView(ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    permission_classes = IsAuthenticated, 

class OrderProductView(ModelViewSet):
    queryset = OrderProduct.objects.all()
    serializer_class = OrderProductSerializer