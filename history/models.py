from django.db import models

class History(models.Model):
    order = models.ForeignKey('orders.Order', models.SET_NULL, related_name='history_order', null=True)
    product = models.ForeignKey('products.Product', models.SET_NULL, related_name='history_product', null=True)
    customers = models.OneToOneField('users.Customer', models.SET_NULL, related_name='history_customers', null=True)
    courier = models.OneToOneField('users.Courier', models.SET_NULL, related_name='history_courier', null=True)

