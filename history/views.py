from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from .models import History
from .serializers import HistorySerializer


class HistoryView(ModelViewSet):
    queryset = History.objects.all()
    serializer_class = HistorySerializer