from django.urls import path
from .views import HistoryView

urlpatterns = [
    path('', HistoryView.as_view({'get': 'list', 'post': 'create'}))
]