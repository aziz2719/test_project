from rest_framework import serializers
from .models import Product
from .models import ProductImage
from .models import ProductMark


class ProductMarkSerializer(serializers.ModelSerializer):
    created_at = serializers.DateTimeField(read_only=True, format='%d.%m.%Y %H:%M')
    class Meta:
        model = ProductMark
        fields = ('__all__')


class ProductSerializer(serializers.ModelSerializer):
    #user_marks = ProductMarkSerializer(many=True)
    class Meta:
        model = Product
        fields = (
            'id', 'name', 'text', 'price', 'category', 'country', 
            'manufacturer', 'raiting',
        )


class ProductImagesSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = ProductImage
        fields = ('__all__')