from rest_framework.permissions import BasePermission, SAFE_METHODS

"""class IsProductUserOrReadOnly(BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True
        elif request.method == 'GET':
            return obj.owner == request.user
        return obj.owner == request.user"""

class TestPermission(BasePermission):
    def has_permission(self, request, view):
        print(request.user)
        print('-'*80)
        if request.method == 'GET':
            return True
        else:
            return request.user.is_superuser or request.user.is_staff
