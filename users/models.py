from django.db import models
from django.contrib.auth.models import AbstractUser

class User(AbstractUser):
    avatar = models.ImageField('Изображение', upload_to='user_image/')
    phone = models.CharField('Номер телефона', max_length=30)
    address = models.CharField('Адрес', max_length=100)

    def __str__(self):
        return self.username


class Customer(models.Model):
    user = models.OneToOneField(User, models.CASCADE, related_name='user_customer', null = True)


class Courier(models.Model):
    user = models.OneToOneField(User, models.CASCADE, related_name='user_courier', null = True)
    car_number = models.CharField('Номер машины', max_length=10, null=True, blank=True)
    rating = models.PositiveSmallIntegerField('Рейтинг', default=0)

    def __str__(self):
        return self.car_number