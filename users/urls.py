from django.urls import path
from .views import UserView
from .views import CustomerView
from .views import CourierView


urlpatterns = [
    path('', UserView.as_view({'get': 'list', 'post': 'create'})),
    path('', CustomerView.as_view({'get': 'list', 'post': 'create'})),
    path('', CourierView.as_view({'get': 'list', 'post': 'create'})),
]