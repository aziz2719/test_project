from rest_framework import serializers
from .models import User
from .models import Customer
from .models import Courier


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('__all__')


class CustomerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Customer
        fields = ('__all__')


class CourierSerializer(serializers.ModelSerializer):

    class Meta:
        model = Courier
        fields = ('__all__')