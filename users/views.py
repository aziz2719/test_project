from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from .serializers import UserSerializer
from .serializers import CustomerSerializer
from .serializers import CourierSerializer
from .models import User
from .models import Customer
from .models import Courier

class UserView(ModelViewSet):
    queryset = User.objects.all()
    serializer_class  = UserSerializer
    lookup_field = 'pk'


class CustomerView(ModelViewSet):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    lookup_field = 'pk'


class CourierView(ModelViewSet):
    queryset = Courier.objects.all()
    serializer_class = CourierSerializer
    lookup_field = 'pk'
