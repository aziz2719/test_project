from django.contrib import admin
from .models import User
from .models import Customer
from .models import Courier


admin.site.register(User)
admin.site.register(Customer)
admin.site.register(Courier)
