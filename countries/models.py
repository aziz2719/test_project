from django.db import models


class Country(models.Model):
    country = models.CharField('Страна', max_length=120)
    
    def __str__(self):
        return self.country

