from django.shortcuts import render
from .serializers import CountrySerializer
from .models import Country
from rest_framework.viewsets import ModelViewSet


class CountryView(ModelViewSet):
    queryset = Country.objects.all()
    serializer_class = CountrySerializer
