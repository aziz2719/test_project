from django.urls import path
from .views import ManufacturerView

urlpatterns = [
    path('', ManufacturerView.as_view({'get': 'list', 'post': 'create'}))
]