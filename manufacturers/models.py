from django.db import models

class Manufacturer(models.Model):
    manufacturer = models.CharField('Производитель', max_length=100)

    def __str__(self):
        return self.manufacturer

