from django.contrib import admin
from django.urls import path, include
from profiles.views import ProfileView
from users.views import UserView
from categories.views import CategoriesView
from users.views import CourierView
from users.views import CustomerView
from history.views import HistoryView
from rest_auth.views import LoginView
from rest_auth.registration.views import RegisterView

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('signin/', LoginView.as_view(), name = 'rest_login'),
    path('signup/', RegisterView.as_view(), name = 'rest_register'),
    path('user/', include('users.urls')),
    path('user/<int:pk>', UserView.as_view({'get': 'retrieve', 'put': 'update'})),
    path('customer/<int:pk>', CustomerView.as_view({'get': 'retrieve', 'put': 'update'})),
    path('courier/<int:pk>', CourierView.as_view({'get': 'retrieve', 'put': 'update'})),
    path('order/', include('orders.urls')),
    path('order_product/', include('orders.urls')),
    path('categories/', include('categories.urls')),
    path('country/', include('countries.urls')),
    path('manufacturer/', include('manufacturers.urls')),
    path('products/', include('products.urls')),
    path('profile/', include('profiles.urls')),
    path('profile/<int:pk>', ProfileView.as_view({'get': 'retrieve', 'put': 'update'})),
    path('history', include('history.urls')),
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
